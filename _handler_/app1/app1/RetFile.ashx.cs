﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace app1
{
	/// <summary>
	/// Summary description for RetFile
	/// </summary>
	public class RetFile : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "application/octet-stream";

			context.Response.AddHeader("Content-Disposition", "attachment; filename=" + "a.zip");

			//Response.Buffer = true;

			context.Response.TransmitFile(context.Server.MapPath("_retFile/1.zip"));

			//Response.Flush();
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}