﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace app1
{
	/// <summary>
	/// Summary description for RetCsv
	/// </summary>
	public class RetCsv : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/csv";
			context.Response.AddHeader(
				"Content-Disposition"
				,

				new System.Net.Mime.ContentDisposition() { Inline = false, FileName = "byPerson.csv" }.ToString()
			);

			Action<string> writeLine = (s) =>
			{
				context.Response.Write(
					s + Environment.NewLine
				);
			};

			foreach (var item in new[] { 1, 2, 3 })
			{
				writeLine(

					string.Join(",", item, "abc")		

				);

			}
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}