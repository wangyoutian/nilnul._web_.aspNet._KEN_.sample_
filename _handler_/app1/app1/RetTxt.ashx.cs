﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace app1
{
	/// <summary>
	/// Summary description for RetTxt
	/// </summary>
	public class RetTxt : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";
			context.Response.Write(new Random().Next(2));
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}